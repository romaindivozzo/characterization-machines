| - | Parts | EU | US | 
| - | - | - | - |
| 1 | Load Cell | #Buying from Jake's Ref | [Jake's Ref](https://www.amazon.com/Pressure-Force-S-type-Sensor-Cable/dp/B01F6IOWDG/) |
| 2 | SparkFun Load Cell Amp HX711 | *In stock at Fablab Digiscope | [Jake's ref](https://www.sparkfun.com/products/13879) | 
| 3 | Power Supply | [MEAN WELL LRS-350 (350W, 24V)](https://www.ledkia.com/fr/acheter-alimentation-interieur-pour-rubans-led/731-bloc-d-alimentation-mean-well-nes-350-24v.html?) | [Jake's Ref](https://www.amazon.com/MEAN-WELL-LRS-350-24-350-4W-Switchable/dp/B013ETVO12/) |
| 4 | Feather |  [Adafruit Feather M4 Express](https://letmeknow.fr/shop/fr/feather/1973-adafruit-feather-m4-express-4060137049415.html) | - |
| 5 | Bearings | [625-ZZ from REPRAP FRANCE](https://www.reprap-france.com/produit/1234568282-roulement-a-billes-625zz) | [Jake's Ref](https://www.vxb.com/ShoppingCart.asp?) |
| 6 | Stepper Motor | [NEMA23 from REPRAP FRANCE](https://www.reprap-france.com/produit/1234568412-moteur-pas-a-pas-nema-23-54-5mm) | [Jake'S Ref](https://www.omc-stepperonline.com/nema-23-bipolar-1-8deg-1-26nm-178-4oz-in-2-8a-2-5v-57x57x56mm-4-wires.html) |
| 7 |Pinions | [GT2 20 TOOTH 6MM from REPRAP FRANCE](https://www.reprap-france.com/produit/379-poulie-gt2-20-dents-pour-courroie-de-6mm-axe-5mm) | [Jake'S Ref](https://www.amazon.com/Saiper-GT2-Teeth-6-35mm-Synchronous/dp/B07MGMBX3N/) |
| 8 |TIMING-BELT (6MM) | [GT2 (6MM)](https://www.imp3d-france.com/article/451/) | [Jake's Ref](https://www.amazon.com/400-2GT-6-Timing-Belt-Closed-Loop/dp/B014U7OSVA/) |
| 9 | SFU1204 (BK/BF, 400MM) | #Buying from Jake's Ref (possible delivery on May 3rd 2021) | [Jake's Ref](https://www.amazon.com/SFU1204-Ballscrew-RM1204-Housing-Machine/dp/B076PCVC8F/#HLCXComparisonWidget_feature_div) |
| 10 | Acrylic Sheet ~6mm is not common | --> Could we use HDPE instead and mill it down to the needed thickness ? | - |
| 11 | Tough PLA | [Tough PLA 2.85MM GREEN](https://www.makershop.fr/filaments-ultimaker/2026-pla-tough-ultimaker-vert.html) | - |
